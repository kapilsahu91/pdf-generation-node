var express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors');
var app = express();
app.use(cors())
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
var deleteFile = false;

var server = app.listen(3000, function () {
    console.log("app running on port.", server.address().port);
});

app.get('/getPdf/:filename', function (req, res) {
    var fs = require('fs');
    var filePath = "/pdfs/" + req.params.filename;

    fs.readFile(__dirname + filePath , function (err,data){
        res.setHeader('Content-disposition', 'attachment; filename=' + req.params.filename);
        res.contentType("application/pdf");
        res.send(data);
        deleteFile = true;
    });
    setImmediate((deleteFile) => {
        fs.unlink(__dirname + filePath, function(error) {
            if (error) {
                throw error;
            }
            console.log('Deleted ',filePath);
        });
      }, 'Deleted '+filePath);
    
});

app.post('/getComparisonSummaryReport', function (req, res) {

    var fs = require('fs');
    var pdf = require('html-pdf');
    var html = fs.readFileSync('raw_summary_page.html', 'utf8');
    var options = { format: 'A3', timeout: 3000000 };
    const jsdom = require("jsdom");
    const { JSDOM } = jsdom;
    const dom = new JSDOM(html);

    var script = dom.window.document.createElement("script");
    script.type = "text/javascript";
    dom.window.document.getElementById("pieChart").innerHTML = req.body.pieChart;
    dom.window.document.getElementById("barChart").innerHTML = req.body.barChart;
    var end = '$scope.isVMC = function(){if($scope.selected ==22 || $scope.selected ==23 || $scope.selected ==24){return true;}else{ return false;}} });';
    var allData = 'app.controller("myCtrl", function($scope,$http) {$scope.hideElement = true;';
    var keyArr = Object.keys(req.body);
    for (let key in keyArr) {
        if (keyArr[key] != "barChart" && keyArr[key] != "pieChart")
            allData += "$scope." + keyArr[key] + " = " + JSON.stringify(req.body[keyArr[key]]) + ';';
    }

    script.innerHTML = allData + end;

    try {
        dom.window.document.body.appendChild(script);
        const dom1 = new JSDOM(dom.window.document.documentElement.innerHTML);
        var fileName = '';
		fs.writeFile("createHtml.html", dom1.window.document.documentElement.innerHTML, function(err) {
			if(err) {
				return console.log(err);
			}
			console.log("The file was saved!");
		}); 
        if (req.body.exportPDFFileName != undefined) {
            fileName = req.body.exportPDFFileName + '.pdf';
        } else {
            fileName = req.body.userProfile.firstName + '-' + req.body.userProfile.lastName + '-' + req.body.hypervisorDatasetName + '.pdf';
            fileName = fileName.replace(/ /g,"_");
        }
        pdf.create(dom1.window.document.documentElement.innerHTML, options).toFile("pdfs/"+fileName, function (err, resp) {
            if (err) return console.log(err);
            console.log({ "fileName": fileName }); // { filename: '/app/businesscard.pdf' }
            res.send({ "fileName": fileName });
        });

    } catch (error) {

    }

});